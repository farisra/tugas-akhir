<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JatimLerek;
use App\BangJarek;
use App\SalJarek;
use App\JatiKulon;
use App\BangJalon;
use App\SalJalon;
// use App\Kecamatan;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function test(){
        return 'in';
    }

    public function jatimLerek(){
        $jatimlerek = JatimLerek::select(DB::raw("ST_AsGeoJSON(ST_Transform(ST_SetSRID(di_jatimlerek.geom,4326),4326))::json As geometry"),
        'di_jatimlerek.nama_di','di_jatimlerek.kewenangan','di_jatimlerek.irigasi')
        ->get();
        // return $jatimlerek;

        $original_data=json_decode($jatimlerek,true);
        foreach ($original_data as $key =>$value){
            $features[]=array(
                'type' => 'Feature',
                'geometry'=>json_decode($value["geometry"], true),
                'properties'=>array('nama_daerah_irigasi'=>$value['nama_di'], 'wewenang'=>$value['kewenangan'],
                            'nama_irigasi'=>$value['irigasi'])
            );
        };
        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

        $data_di_jatimlerek = json_encode($allfeatures,JSON_PRETTY_PRINT);
//        return view(dd($data_desa));
        return $data_di_jatimlerek;
    }

    public function bangjatimlerek(){
        $data_bangjarek = BangJarek::select(DB::raw("ST_AsGeoJSON(ST_Transform(ST_SetSRID(bang_jarek.geom,4326),4326))::json As geometry"))
            ->get();
        // return $jatimlerek;

        $original_data=json_decode($data_bangjarek,true);
        foreach ($original_data as $key =>$value){
            $features[]=array(
                'type' => 'Feature',
                'geometry'=>json_decode($value["geometry"], true),
                'properties'=>array('nama_bang_jarek'=>'nama')
            );
        };
        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

        $data_bangjarek = json_encode($allfeatures,JSON_PRETTY_PRINT);
//        return view(dd($data_desa));
        return $data_bangjarek;
    }

    public function salJarek(){
        $data_saljarek = SalJarek::select(DB::raw("ST_AsGeoJSON(ST_Transform(ST_SetSRID(sal_jatimlerek.geom,4326),4326))::json As geometry"),
        'sal_jatimlerek.id','sal_jatimlerek.sungai_sal')
            ->get();
        // return $jatimlerek;

        $original_data=json_decode($data_saljarek,true);
        foreach ($original_data as $key =>$value){
            $features[]=array(
                'type' => 'Feature',
                'geometry'=>json_decode($value["geometry"], true),
                'properties'=>array('nama_sal_jarek'=>$value['id'], 'sungai_jarek'=>$value['sungai_sal'])
            );
        };
        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

        $data_saljarek = json_encode($allfeatures,JSON_PRETTY_PRINT);
//        return view(dd($data_desa));
        return $data_saljarek;
    }

    public function jatiKulon(){
        $jatikulon = JatiKulon::select(DB::raw("ST_AsGeoJSON(ST_Transform(ST_SetSRID(di_jatikulon.geom,4326),4326))::json As geometry"),
        'di_jatikulon.propinsi','di_jatikulon.irigasi','di_jatikulon.objek','di_jatikulon.keterangan')
            ->get();
        // return $jatimlerek;

        $original_data=json_decode($jatikulon,true);
        foreach ($original_data as $key =>$value){
            $features[]=array(
                'type' => 'Feature',
                'geometry'=>json_decode($value["geometry"], true),
                'properties'=>array('nama_jatikulon'=>$value['propinsi'], 'nama_irigasi'=>$value['irigasi'],
                            'nama_objek'=>$value['objek'],'keterangan'=>$value['keterangan'])
            );
        };
        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

        $data_di_jatikulon = json_encode($allfeatures,JSON_PRETTY_PRINT);
//        return view(dd($data_desa));
        return $data_di_jatikulon;
    }
    public function bangJalon(){
        $data_bangjalon = BangJalon::select(DB::raw("ST_AsGeoJSON(ST_Transform(ST_SetSRID(bang_jatikulon.geom,4326),4326))::json As geometry"),
        'bang_jatikulon.nama_bangu','bang_jatikulon.desa','bang_jatikulon.kecamatan','bang_jatikulon.kota_kab',
        'bang_jatikulon.kondisi','bang_jatikulon.sumberdata')
            ->get();
        // return $jatimlerek;

        $original_data=json_decode($data_bangjalon,true);
        foreach ($original_data as $key =>$value){
            $features[]=array(
                'type' => 'Feature',
                'geometry'=>json_decode($value["geometry"], true),
                'properties'=>array('bangunan'=>$value['nama_bangu'], 'desa'=>$value['desa'],
                'kecamatan'=>$value['kecamatan'],'kota_kab'=>$value['kota_kab'],'kondisi'=>$value['kondisi']
                ,'sumberdata'=>$value['sumberdata'])
            );
        };
        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

        $data_bangjalon = json_encode($allfeatures,JSON_PRETTY_PRINT);
//        return view(dd($data_desa));
        return $data_bangjalon;
    }
    public function salJalon(){
        $data_saljalon = SalJalon::select(DB::raw("ST_AsGeoJSON(ST_Transform(ST_SetSRID(sal_jatikulon.geom,4326),4326))::json As geometry"))
        ->get();
        // return $jatimlerek;

        $original_data=json_decode($data_saljalon,true);
        foreach ($original_data as $key =>$value){
            $features[]=array(
                'type' => 'Feature',
                'geometry'=>json_decode($value["geometry"], true),
                'properties'=>array('nama_sal_jalon'=>'nama')
            );
        };
        $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

        $data_saljalon = json_encode($allfeatures,JSON_PRETTY_PRINT);
//        return view(dd($data_desa));
        return $data_saljalon;
    }
//      public function Kecamatan(){
//          $data_kecamatan = Kecamatan::select(DB::raw("ST_AsGeoJSON(ST_Transform(ST_SetSRID(kecamatan.geom,4326),4326))::json As geometry"),
//          'kecamatan.kabupaten','kecamatan.kecamatan')
//          ->get();
//          // return $jatimlerek;

//          $original_data=json_decode($data_kecamatan,true);
//          foreach ($original_data as $key =>$value){
//              $features[]=array(
//                  'type' => 'Feature',
//                  'geometry'=>json_decode($value["geometry"], true),
//                  'properties'=>array('nama_kabupaten'=>$value['kabupaten'], 'nama_kecamatan'=>$value['kecamatan'])
//              );
//          };
//          $allfeatures = array('type' => 'FeatureCollection', 'features' => $features);

//          $data_kecamatan = json_encode($allfeatures,JSON_PRETTY_PRINT);
//  //        return view(dd($data_desa));
//          return $data_kecamatan;
//      }

}

