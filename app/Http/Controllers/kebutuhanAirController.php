<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kebutuhan_Air;
use App\DaerahIrigasi;
use App\Fase;

class KebutuhanAirController extends Controller
{
    public function index(){
        $irigasi = $this->getDataAnalisa(1 , 1546189146, 1577750362);
        $jatikulon = $this->getDataAnalisa(2 , 1546189146, 1577750362);
        $menturus = $this->getDataAnalisa(3 , 1546189146, 1577750362);

        $irigasi = $this->analisaData($irigasi);
        $jatikulon = $this->analisaData($jatikulon);
        $menturus = $this->analisaData($menturus);

        $fase =$this->getfase();

        return view('index', compact('irigasi','jatikulon','menturus','fase'));
    }

    public function getDataAnalisa($daerah_irigasi, $startDate, $endDate){
        $irigasi = Kebutuhan_Air::select('kebutuhan_air.id','kebutuhan_air.daerah_irigasi_id','kebutuhan_air.fase_id',
        'kebutuhan_air.bulan','kebutuhan_air.kebutuhan','kebutuhan_air.debit')
        ->join('daerah_irigasi','daerah_irigasi.id','=','daerah_irigasi_id')
        ->join('fase','fase.id','=','fase_id')
        ->orderBy('kebutuhan_air.id','asc')
        ->whereBetween('kebutuhan_air.bulan', [$startDate, $endDate])
        ->where('kebutuhan_air.daerah_irigasi_id', $daerah_irigasi )
        ->get();
        return $irigasi;
    }

    public function analisaData($data){
        $temp_air = 0;
        $temp_debit = 0;
        $finalData = [];
        foreach ($data as $i) {
            $temp_air += $i->kebutuhan;
            $temp_debit += $i->debit;

            $finalDataIrigasi = new \StdClass();
            $finalDataIrigasi->id = $i->id;
            $finalDataIrigasi->bulan = date("M Y", $i->bulan);
            $finalDataIrigasi->kebutuhan = $i->kebutuhan;
            $finalDataIrigasi->debit = $i->debit;
            $finalDataIrigasi->fase_id = $i->fase_id;
            $finalDataIrigasi->komulatif_air = $temp_air;
            $finalDataIrigasi->komulatif_debit = $temp_debit;
            $finalDataIrigasi->analisa =  $temp_debit - $temp_air;
            $finalDataIrigasi->daerah_irigasi_id =  $i->daerah_irigasi_id;
            $finalData[] = $finalDataIrigasi;
        }
        return $finalData;
    }

    public function getLastData($irigasi_id){
        $data = Kebutuhan_Air::where('daerah_irigasi_id', $irigasi_id)->get()->take(40);
        // $data = json_decode(json_encode($this->analisaData($data)), true); //stdclass to array
        $data = $this->analisaData($data); //stdclass to array

        // $result = [];
        // foreach ($data as $val) {
        //     if(array_key_exists("bulan", $val)){
        //         $result[$val["bulan"]][] = $val;
        //     }else {
        //         $result[""][] = $val;
        //     }
        // }
        return $data;
    }

    public function store(Request $request)
    {
        // $save=new Kebutuhan_Air();
        // $save->bulan=1;
        // $save->fase_id=1;
        // $save->kebutuhan=1;
        // $save->debit=1;
        // $save->daerah_irigasi_id=2;

        // if($save->save()){
        //     return "saved";
        // }else{
        //     return "failed";
        // }

        // insert data ke table pegawai
        DB::table('kebutuhan_air')->insert([
            'bulan' =>$request->bulan,
            'fase_id' =>$request->fase,
            'kebutuhan' =>$request->kebutuhan_air,
            'debit' =>$request->debit,
            'daerah_irigasi_id' =>$request->daerah_irigasi
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/');
    }

    public function edit($id)
    {
        // mengambil data tanlong berdasarkan id yang dipilih
        $kebutuhan_air = DB::table('kebutuhan_air')->where('id',$id)->first();
        $kebutuhan_air = Kebutuhan_Air::select('kebutuhan_air.*')
            ->join('daerah_irigasi','daerah_irigasi.id','=','daerah_irigasi_id')
            ->join('fase','fase.id','=','fase_id')
            ->where('id',$id)->first();
        // passing data tanlong yang didapat ke view edit.blade.php
        return response()->json($kebutuhan_air);
    }

    public function update(Request $request, $id)
    {
        DB::table('kebutuhan_air')->where('id',$id)->update([
            'bulan' =>$request->bulan,
            'fase_id' =>$request->fase,
            'kebutuhan' =>$request->kebutuhan_air,
            'debit' =>$request->debit,
            'daerah_irigasi_id' =>$request->daerah_irigasi
        ]);
        // alihkan halaman ke halaman tanlong
        return redirect('/');
    }

    public function getFase(){
        return Fase::all();
    }

    public function getdata_by_id($id){
        return Kebutuhan_Air::find($id)->get()->first();
    }

    public function deleteById($id){
        $data = Kebutuhan_Air::find($id);
        if($data->delete()){
            return "200";
        }
        return "400";
    }
    public function index_user(){
        $irigasi = $this->getDataAnalisa(1 , 1546189146,1562433949);
        $jatikulon = $this->getDataAnalisa(2 , 1546189146, 1556643546);

        $irigasi = $this->analisaData($irigasi);
        $jatikulon = $this->analisaData($jatikulon);

        $fase =$this->getfase();

        return view('user', compact('irigasi','jatikulon','fase'));
    }
}
