<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Maps</title>

    <!-- Fontfaces CSS-->
    <link rel="stylesheet" href=" {{ asset('../assets/leaflet.css') }} ">
    <script src="{{ asset('../assets/leaflet.js') }}"></script>
    <link href="{{ asset('../assets/css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('../assets/vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/data-table/dataTables.bootstrap.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/js/jquery.dataTables.min.js') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/js/dataTables.bootstrap.js') }}" rel="stylesheet" media="all">


    <!-- Vendor CSS-->
    <link href="{{ asset('../assets/vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/vendor/vector-map/jqvmap.min.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('../assets/css/theme.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('../assets/css/table.css') }}" rel="stylesheet" media="all">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">


    <style>
        table, td, th{
            border: 1px solid black;
        }
    </style>

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <img src="{{ asset('../assets/imagess/icon/logo.png') }}" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Pages</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                {{--  <li>
                                    <a href="login.html">Login</a>
                                </li>
                                <li>
                                    <a href="register.html">Register</a>
                                </li>
                                <li>
                                    <a href="forget-pass.html">Forget Password</a>
                                </li>  --}}
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="{{ asset('../assets/imagess/icon/13.png') }}"  alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/grafik') }}">
                                <i class="fas fa-chart-bar"></i>Grafik</a>
                        </li>
                        {{--  <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Pages</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="login.html">Login</a>
                                </li>
                                <li>
                                    <a href="register.html">Register</a>
                                </li>
                                <li>
                                    <a href="forget-pass.html">Forget Password</a>
                                </li>
                            </ul>
                        </li>  --}}
                    </ul>
                </nav>
            </div>
        </aside>

        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                {{--  <input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button>  --}}
                            </form>
                        <div class="header-button">
                                {{--  <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-comment-more"></i>
                                        <span class="quantity">1</span>
                                        <div class="mess-dropdown js-dropdown">
                                            <div class="mess__title">
                                                <p>You have 2 news message</p>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="{{ asset('../assets/imagess/icon/avatar-06.jpg') }}" alt="Michelle Moreno" />
                                                </div>
                                                <div class="content">
                                                    <h6>Michelle Moreno</h6>
                                                    <p>Have sent a photo</p>
                                                    <span class="time">3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="{{ asset('../assets/imagess/icon/avatar-04.jpg') }}" alt="Diane Myers" />
                                                </div>
                                                <div class="content">
                                                    <h6>Diane Myers</h6>
                                                    <p>You are now connected on message</p>
                                                    <span class="time">Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="mess__footer">
                                                <a href="#">View all messages</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-email"></i>
                                        <span class="quantity">1</span>
                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>You have 3 New Emails</p>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="{{ asset('../assets/imagess/icon/avatar-06.jpg') }}" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, 3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="{{ asset('../assets/imagess/icon/avatar-05.jpg') }}" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="{{ asset('../assets/imagess/icon/avatar-04.jpg') }}" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, April 12,,2018</span>
                                                </div>
                                            </div>
                                            <div class="email__footer">
                                                <a href="#">See all emails</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>  --}}
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="{{ asset('../assets/imagess/icon/user.png') }}" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="{{ asset('../assets/imagess/icon/user.png') }}" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">{{Auth::user()->name}}</a>
                                                    </h5>
                                                    <span class="email">{{Auth::user()->email}}</span>
                                                </div>
                                            </div>
                                            {{--  <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                </div>
                                            </div>  --}}
                                            <div class="account-dropdown__footer">
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                                  document.getElementById('logout-form').submit();">
                                                      <i class="zmdi zmdi-power"></i>Logout
                                                    </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                     @csrf
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->

            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- MAP DATA-->
                                <div class="map-data m-b-40">
                                    <h3 class="title-3 m-b-30">
                                        <i class="zmdi zmdi-map"></i>irrigation area</h3>
                                    <div class="filters">
                                        <div class="rs-select2--dark rs-select2--md m-r-10 rs-select2--border">
                                            <select id="size_select" class="js-select2" name="property">
                                                <option value="option1">Jatimlerek</option>
                                                <option value="option2">Jatikulon</option>
                                                <option value="option3">Menturus</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                    </div>
                                    <div class="map-wrap m-t-45 m-b-20">
                                        <div id="option1" class="size_chart">
                                            <div id="vmap" style="height: 450px;"></div>
                                            <div style="margin:50px"></div>
                                            <div>
                                                    <button id="btn-add" type="button"
                                                    class="btn btn-success btn-add" data-dismiss="modal"
                                                    style="margin-top: 15px" data-toggle="modal" data-target="#myModal"
                                                    target="/tambah" data-id="{{$irigasi[0]->daerah_irigasi_id }}">Tambah data</button>
                                                <table class="datatable-custom">
                                                    <thead>
                                                        <tr style="text-align:center">
                                                            {{--  <th class="col-sm-1">Id</th>  --}}
                                                            <th>Bulan</th>
                                                            <th>Fase</th>
                                                            <th >Kebutuhan Air</th>
                                                            <th >Kumulatif Kebutuhan Air</th>
                                                            <th >Debit Probabilitas
                                                                50-65%
                                                            </th>
                                                            <th>Kumulatif Debit Probabilitas
                                                                50-65%
                                                            </th>
                                                            <th> Analisa Ketersediaan Air</th>
                                                            <th> Action </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach( $irigasi as $t )
                                                        <tr style="text-align:center">
                                                            {{--  <td>{{ $t->id }}</td>  --}}
                                                            <td>{{ $t->bulan }}</td>
                                                            <td>{{ $t->fase_id }}</td>
                                                            <td>{{ $t->kebutuhan }}</td>
                                                            <td>{{ $t->komulatif_air }}</td>
                                                            <td>{{ $t->debit }}</td>
                                                            <td>{{ $t->komulatif_debit }}</td>
                                                            <td>{{ $t->analisa }}</td>
                                                            <td style="white-space:nowrap">
                                                                    <button type="button" class="btn btn-primary edit-modal"  data-id="{{ $t->id }}"
                                                                            data-toggle="modal" data-target="#myModal" target="/update">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </button>

                                                                    <button type="button" class="btn btn-danger btn-delete" data-id="{{ $t->id }}" data-toggle="modal">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </button>
                                                            </td>
                                                        </tr>
                                                     @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div id="option2" class="size_chart">
                                            <div id="vmap1" style="height: 450px;"></div>
                                            <div style="margin:50px"></div>
                                            <div>
                                                    <button id="btn-add" type="button"
                                                    class="btn btn-success btn-add" data-dismiss="modal"
                                                    style="margin-top: 15px" data-toggle="modal" data-target="#myModal"
                                                    target="/tambah" data-id="{{$jatikulon[0]->daerah_irigasi_id }}">Tambah data</button>
                                                <table class="datatable-custom">
                                                    <thead>
                                                        <tr style="text-align:center">
                                                            {{--  <th class="col-sm-1">Id</th>  --}}
                                                            <th>Bulan</th>
                                                            <th>Fase</th>
                                                            <th >Kebutuhan Air</th>
                                                            <th >Kumulatif Kebutuhan Air</th>
                                                            <th >Debit Probabilitas
                                                                50-65%
                                                            </th>
                                                            <th>Kumulatif Debit Probabilitas
                                                                50-65%
                                                            </th>
                                                            <th> Analisa Ketersediaan Air</th>
                                                            <th> Action </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach( $jatikulon as $t )
                                                        <tr style="text-align:center">
                                                            {{--  <td>{{ $t->id }}</td>  --}}
                                                            <td>{{ $t->bulan }}</td>
                                                            <td>{{ $t->fase_id }}</td>
                                                            <td>{{ $t->kebutuhan }}</td>
                                                            <td>{{ $t->komulatif_air }}</td>
                                                            <td>{{ $t->debit }}</td>
                                                            <td>{{ $t->komulatif_debit }}</td>
                                                            <td>{{ $t->analisa }}</td>
                                                            <td style="white-space:nowrap">
                                                                    <button type="button" class="btn btn-primary edit-modal"  data-id="{{ $t->id }}"
                                                                            data-toggle="modal" data-target="#myModal" target="/update">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </button>

                                                                    <button type="button" class="btn btn-danger btn-delete" data-id="{{ $t->id }}" data-toggle="modal">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </button>
                                                            </td>
                                                        </tr>
                                                     @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div id="option3" class="size_chart">
                                                <div id="vmap2" style="height: 450px;"></div>
                                                <div style="margin:50px"></div>
                                                <div>
                                                    {{--  <button id="btn-add" type="button"
                                                    class="btn btn-success btn-add" data-dismiss="modal"
                                                    style="margin-top: 15px" data-toggle="modal" data-target="#myModal"
                                                    target="/tambah" data-id="{{$jatikulon[0]->daerah_irigasi_id }}">Tambah data</button>  --}}
                                                <table class="datatable-custom">
                                                    <thead>
                                                        <tr style="text-align:center">
                                                            {{--  <th class="col-sm-1">Id</th>  --}}
                                                            <th>Bulan</th>
                                                            <th>Fase</th>
                                                            <th >Kebutuhan Air</th>
                                                            <th >Kumulatif Kebutuhan Air</th>
                                                            <th >Debit Probabilitas
                                                                50-65%
                                                            </th>
                                                            <th>Kumulatif Debit Probabilitas
                                                                50-65%
                                                            </th>
                                                            <th> Analisa Ketersediaan Air</th>
                                                            <th> Action </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach( $menturus as $t )
                                                        <tr style="text-align:center">
                                                            {{--  <td>{{ $t->id }}</td>  --}}
                                                            <td>{{ $t->bulan }}</td>
                                                            <td>{{ $t->fase_id }}</td>
                                                            <td>{{ $t->kebutuhan }}</td>
                                                            <td>{{ $t->komulatif_air }}</td>
                                                            <td>{{ $t->debit }}</td>
                                                            <td>{{ $t->komulatif_debit }}</td>
                                                            <td>{{ $t->analisa }}</td>
                                                            <td style="white-space:nowrap">
                                                                    <button type="button" class="btn btn-primary edit-modal"  data-id="{{ $t->id }}"
                                                                            data-toggle="modal" data-target="#myModal" target="/update">
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </button>

                                                                    <button type="button" class="btn btn-danger btn-delete" data-id="{{ $t->id }}" data-toggle="modal">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </button>
                                                            </td>
                                                        </tr>
                                                     @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            </div>
                                    </div>
                                </div>
                                <!-- END MAP DATA-->

                                {{-- CRUD --}}
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title custom-title">Edit Data</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <form id="form-tanlong" action="/store " method="post">
                {{ csrf_field() }}
                    <div class="modal-body">
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Bulan</label>
                                        <input class="form-control" type="number" name="bulan">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Fase</label>
                                        <select class="form-control select-kecamatan" id="fase" name="fase">
                                            <option value="0" disable="true" selected="true"> Pilih Fase </option>
                                            @foreach( $fase as $k )
                                                <option value="{{ $k->id }}"> {{ $k->keterangan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Kebutuhan Air</label>
                                        <input class="form-control" id="kebutuhan_air" name="kebutuhan_air">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Debit Probabilitas</label>
                                        <input class="form-control" type='double' name="debit">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="hidden" name="daerah_irigasi">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
            </form>

    <!-- Jquery JS-->
    <script src="{{ asset('../assets/vendor/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('../assets/vendor/bootstrap-4.1/popper.min.js') }}"></script>
    <script src="{{ asset('../assets/vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
    <!-- Vendor JS       -->
    <script src="{{ asset('../assets/vendor/slick/slick.min.js') }}">
    </script>
    <script src="{{ asset('../assets/vendor/wow/wow.min.js') }}"></script>
    <script src="{{ asset('../assets/vendor/animsition/animsition.min.js') }}"></script>
    <script src="{{ asset('../assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
    </script>
    <script src="{{ asset('../assets/vendor/counter-up/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('../assets/vendor/counter-up/jquery.counterup.min.js') }}">
    </script>
    <script src="{{ asset('../assets/vendor/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('../assets/vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('../assets/vendor/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('../assets/vendor/select2/select2.min.js') }}">
    </script>
    <script src="{{ asset('../assets/vendor/vector-map/jquery.vmap.js') }}"></script>
    <script src="{{ asset('../assets/vendor/vector-map/jquery.vmap.min.js') }}"></script>

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


    <!-- Main JS-->
    <script src="{{ asset('../assets/js/main.js') }}"></script>
    <script>
        var mymap = L.map('vmap').setView([-7.4435834,112.2577694], 12);
        var mymap1 = L.map('vmap1').setView([-7.4652728,112.4511616], 12);
        var mymap2 = L.map('vmap2').setView([-7.4442054,112.3090012], 12);


        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZmFyaXNyYWZpMTUiLCJhIjoiY2p4M2JndWFwMDFnYjQ5a3Rkbm1xOXd2eSJ9.b6Usvg6riot0AuSk4OmxSQ', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZmFyaXNyYWZpMTUiLCJhIjoiY2p4M2JndWFwMDFnYjQ5a3Rkbm1xOXd2eSJ9.b6Usvg6riot0AuSk4OmxSQ'
        }).addTo(mymap);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZmFyaXNyYWZpMTUiLCJhIjoiY2p4M2JndWFwMDFnYjQ5a3Rkbm1xOXd2eSJ9.b6Usvg6riot0AuSk4OmxSQ', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZmFyaXNyYWZpMTUiLCJhIjoiY2p4M2JndWFwMDFnYjQ5a3Rkbm1xOXd2eSJ9.b6Usvg6riot0AuSk4OmxSQ'
        }).addTo(mymap1);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZmFyaXNyYWZpMTUiLCJhIjoiY2p4M2JndWFwMDFnYjQ5a3Rkbm1xOXd2eSJ9.b6Usvg6riot0AuSk4OmxSQ', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZmFyaXNyYWZpMTUiLCJhIjoiY2p4M2JndWFwMDFnYjQ5a3Rkbm1xOXd2eSJ9.b6Usvg6riot0AuSk4OmxSQ'
        }).addTo(mymap2);

        $.ajax({
            type : 'get',
            url : '/api/jatimlerek',
            dataType: 'json',
            success : function(data){
                L.geoJSON(data).addTo(mymap);
            }
        })
        $.ajax({
            type : 'get',
            url : '/api/bangjarek',
            dataType: 'json',
            success : function(data){
                L.geoJSON(data).addTo(mymap);
            }
        })
        $.ajax({
            type : 'get',
            url : '/api/saljarek',
            dataType: 'json',
            success : function(data){
                L.geoJSON(data).addTo(mymap);
            }
        })
        $.ajax({
            type : 'get',
            url : '/api/jatikulon',
            dataType: 'json',
            success : function(data){
                L.geoJSON(data).addTo(mymap1);
            }
        })
        $.ajax({
            type : 'get',
            url : '/api/bangjalon',
            dataType: 'json',
            success : function(data){
                L.geoJSON(data,{
                    onEachFeature: onEachFeature
                }).addTo(mymap1);
            function onEachFeature(feature, layer) {
                var popupContent =  "<div class='popup-body'> Nama Bendungan : "+ feature.properties.bangunan+"</div>"+
                                    "<div class='popup-body'> Desa : "+ feature.properties.desa+"</div>"+
                                    "<div class='popup-body'> Kecamatan : "+ feature.properties.kecamatan+"</div>"+
                                    "<div class='popup-body'> Kabupaten : "+ feature.properties.kota_kab+"</div>"+
                                    "<div class='popup-body'> Kondisi : "+ feature.properties.kondisi+"</div>"+
                                    "<div class='popup-body'> Sumber Data: "+ feature.properties.sumberdata+"</div>"
                layer.bindPopup(popupContent);
            }}
        })
        $.ajax({
            type : 'get',
            url : '/api/saljalon',
            dataType: 'json',
            success : function(data){
                L.geoJSON(data).addTo(mymap1);
            }
        })
        //$.ajax({
        //    type : 'get',
        //    url : '/api/kecamatan',
        //    dataType: 'json',
        //    success : function(data){
        //        L.geoJSON(data).addTo(mymap);
        //   }
        //})
    </script>

    <script>
        $(document).ready(function(){
            // DataTable
            var table = $('.datatable-custom').DataTable({
                "lengthChange": false,
                "ordering": false,
                "paging":false
            });


            $(".edit-modal").click(function(e) {
                id = $(this).attr('data-id');
                $('.custom-title').text("Edit Data")

                console.log(id);
                $.get('/api/kebutuhan-air/' + id, function(data) {
                     console.log(data);
                     $("#form-tanlong input[name='bulan']").val(data.bulan)
                     $(".select-kecamatan option[value='" + data.id + "']").attr('selected', 'selected');

                     $("#form-tanlong input[name='kebutuhan_air']").val(data.kebutuhan)
                     $("#form-tanlong input[name='debit']").val(data.debit)
                     $("#form-tanlong input[name='daerah_irigasi']").val(data.daerah_irigasi_id)
                 })
                 $('#form-tanlong').attr('action', '/update/' + id);
            });

            $('.btn-delete').click(function(e){
                id = $(this).attr('data-id');

                $.get('/api/delete/'+id, function(data){
                    location.href = "/";
                })
            })
            $(".btn-add").click(function () {
                $('.custom-title').text("Tambah Data")

                $('#form-tanlong').attr('action', '/store');
                $("#form-tanlong input[name='daerah_irigasi']").val( $(this).attr('data-id'))

            })
        })
    </script>
</body>

</html>
<!-- end document-->
