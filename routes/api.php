<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/jatimlerek', 'HomeController@jatimLerek')->name('jatimlerek');
Route::get('/bangjarek', 'HomeController@bangjatimlerek')->name('bangjarek');
Route::get('/saljarek', 'HomeController@saljarek')->name('saljarek');
Route::get('/jatikulon', 'HomeController@jatikulon')->name('jatikulon');
Route::get('/bangjalon', 'HomeController@bangjalon')->name('bangjalon');
Route::get('/saljalon', 'HomeController@saljalon')->name('saljalon');
Route::get('/kecamatan', 'HomeController@kecamatan')->name('kecamatan');
Route::get('/getLastData/{id}', 'KebutuhanAirController@getLastData');
Route::get('/kebutuhan-air/{id}', 'KebutuhanAirController@getdata_by_id');
Route::get('/delete/{id}', 'KebutuhanAirController@deleteById');
