<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->middleware('auth');
Route::get('/user', function () {
    return view('user');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/', 'KebutuhanAirController@index')->name('table')->middleware('auth');
Route::get('/user', 'KebutuhanAirController@index_user')->name('table');
Route::post('/store', 'KebutuhanAirController@store')->name('table');
Route::get('/grafik', 'GrafikController@looking');
Route::get('/usergrafik', 'GrafikController@looking_user');
Route::get('/edit/{id}','KebutuhanAirController@edit');
Route::post('/update/{id}','KebutuhanAirController@update');
